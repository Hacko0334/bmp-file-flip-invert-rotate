#include "bitmap.h"

int main(int argc, char** argv){

    char          bmpInputFilePath[SHORT_STR_LENGHT];
    char          bmpOutputFilePath[SHORT_STR_LENGHT];
    char          instruction[SHORT_STR_LENGHT];
    BMPFILEHEADER bitmapFileHeader;
    BMPINFOHEADER bitmapInfoHeader;
    Pixel_1**     imageBuffer_1;
    Pixel_4**     imageBuffer_4;
    Pixel_8**     imageBuffer_8;
    Pixel_16**    imageBuffer_16;
    UINT8*        spaceBetweenHeadersAndImage;
    FILE*         inputFilePtr;
    UINT8         result = RESULT_SUCCESS;
    UINT8         option;

    readPaths(argc, argv, bmpInputFilePath, bmpOutputFilePath);

    inputFilePtr = LoadFileMetaData(bmpInputFilePath, &bitmapInfoHeader, &bitmapFileHeader, &spaceBetweenHeadersAndImage);
    if(NULL == inputFilePtr){
        fprintf(stderr, "Problem while loading BMP file!");
        return 1;
    }

    printFileHeaders(bitmapFileHeader, bitmapInfoHeader);

    if(1 == bitmapInfoHeader.bitsPerPixel){
        imageBuffer_1 = Load1BppPixelMatrix(inputFilePtr, &bitmapInfoHeader, &bitmapFileHeader);
        if(NULL == imageBuffer_1){
            fprintf(stderr, "Problem while loading pixel matrix!");
            return 1;
        }
    }
    else if(4 == bitmapInfoHeader.bitsPerPixel){
        imageBuffer_4 = Load4BppPixelMatrix(inputFilePtr, &bitmapInfoHeader, &bitmapFileHeader);
        if(NULL == imageBuffer_4){
            fprintf(stderr, "Problem while loading pixel matrix!");
            return 1;
        }
    }
    else if(8 == bitmapInfoHeader.bitsPerPixel){
        imageBuffer_8 = Load8BppBmpPixelMarix(inputFilePtr, &bitmapInfoHeader, &bitmapFileHeader);
        if(NULL == imageBuffer_8){
            fprintf(stderr, "Problem while loading pixel matrix!");
            return 1;
        }
    }
    else if(16 == bitmapInfoHeader.bitsPerPixel){
        imageBuffer_16 = Load16BppBmpPixelMarix(inputFilePtr, &bitmapInfoHeader, &bitmapFileHeader, &spaceBetweenHeadersAndImage);
        if(NULL == imageBuffer_16){
            fprintf(stderr, "Problem while loading pixel matrix!");
            return 1;
        }
    }
    else{
        fprintf(stderr, "Not supported file format!");
        return 1;
    }

    if(argc < 4){

        printMenu();

        do
        {
            scanf("%d", &option);
            switch (option)
            {
            case 1:
                if(1 == bitmapInfoHeader.bitsPerPixel){
                    flipImage1bpp(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(4 == bitmapInfoHeader.bitsPerPixel){
                    flipImage4bpp(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(8 == bitmapInfoHeader.bitsPerPixel){
                    flipImage8bpp(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(16 == bitmapInfoHeader.bitsPerPixel){
                    flipImage16bpp(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                break;
            case 2:
                if(1 == bitmapInfoHeader.bitsPerPixel){
                    InvertColors1bpp(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(4 == bitmapInfoHeader.bitsPerPixel){
                    InvertColors4bpp(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(8 == bitmapInfoHeader.bitsPerPixel){
                    InvertColors8bpp(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                if(16 == bitmapInfoHeader.bitsPerPixel){
                    InvertColors16bpp(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                break;
            case 3:
                if(1 == bitmapInfoHeader.bitsPerPixel){
                    imageBuffer_1 = rotate1bppImage(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_1){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
                if(4 == bitmapInfoHeader.bitsPerPixel){
                    imageBuffer_4 = rotate4bppImage(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_4){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
                if(8 == bitmapInfoHeader.bitsPerPixel){
                    imageBuffer_8 = rotate8bppImage(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_8){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
                if(16 == bitmapInfoHeader.bitsPerPixel){
                    imageBuffer_16 = rotate16bppImage(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_16){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
                break;
            case 4:
                if(argc < 3){
                    printf("Please enter output file path: ");
                    scanf("%s", bmpOutputFilePath);
                }

                if(1 == bitmapInfoHeader.bitsPerPixel){
                    result = write1bppImageToFile(bmpOutputFilePath, imageBuffer_1, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
                    if(RESULT_FAIL == result){
                        fprintf(stderr, "Fail writing the image to file!");
                    }
                    free(imageBuffer_1);
                }
                if(4 == bitmapInfoHeader.bitsPerPixel){
                    result = write4bppImageToFile(bmpOutputFilePath, imageBuffer_4, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
                    if(RESULT_FAIL == result){
                        fprintf(stderr, "Fail writing the image to file!");
                    }
                    free(imageBuffer_4);
                }
                if(8 == bitmapInfoHeader.bitsPerPixel){
                    result = write8bppImageToFile(bmpOutputFilePath, imageBuffer_8, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
                    if(RESULT_FAIL == result){
                        fprintf(stderr, "Fail writing the image to file!");
                    }
                    free(imageBuffer_8);
                }
                if(16 == bitmapInfoHeader.bitsPerPixel){
                    result = write16bppImageToFile(bmpOutputFilePath, imageBuffer_16, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
                    if(RESULT_FAIL == result){
                        fprintf(stderr, "Fail writing the image to file!");
                    }
                    free(imageBuffer_16);
                }
                break;
            default:
                printf("Invalid option, try again.");
                break;
            }
        } while (option != 4);
    }
    else{
        if(1 == bitmapInfoHeader.bitsPerPixel){
            for (size_t i = 3; i < argc; i++)
            {
                if(0 == strcmp(argv[i], "FLIP") || 0 == strcmp(argv[i], "flip")){
                    flipImage1bpp(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "INVERT") || 0 == strcmp(argv[i], "invert")){
                    InvertColors1bpp(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "ROTATE") || 0 == strcmp(argv[i], "rotate")){
                    imageBuffer_1 = rotate1bppImage(imageBuffer_1, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_1){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
            }

            write1bppImageToFile(bmpOutputFilePath, imageBuffer_1, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
            free(imageBuffer_1);
        }
        else if(4 == bitmapInfoHeader.bitsPerPixel){
            for (size_t i = 3; i < argc; i++)
            {
                if(0 == strcmp(argv[i], "FLIP") || 0 == strcmp(argv[i], "flip")){
                    flipImage4bpp(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "INVERT") || 0 == strcmp(argv[i], "invert")){
                    InvertColors4bpp(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "ROTATE") || 0 == strcmp(argv[i], "rotate")){
                    imageBuffer_4 = rotate4bppImage(imageBuffer_4, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_4){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
            }

            write4bppImageToFile(bmpOutputFilePath, imageBuffer_4, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
            free(imageBuffer_4);
        }
        else if(8 == bitmapInfoHeader.bitsPerPixel){
            for (size_t i = 3; i < argc; i++)
            {
                if(0 == strcmp(argv[i], "FLIP") || 0 == strcmp(argv[i], "flip")){
                    flipImage8bpp(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "INVERT") || 0 == strcmp(argv[i], "invert")){
                    InvertColors8bpp(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "ROTATE") || 0 == strcmp(argv[i], "rotate")){
                    imageBuffer_8 = rotate8bppImage(imageBuffer_8, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_8){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
            }

            write8bppImageToFile(bmpOutputFilePath, imageBuffer_8, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
            free(imageBuffer_8);
        }
        else if(16 == bitmapInfoHeader.bitsPerPixel){
            for (size_t i = 3; i < argc; i++)
            {
                if(0 == strcmp(argv[i], "FLIP") || 0 == strcmp(argv[i], "flip")){
                    flipImage16bpp(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "INVERT") || 0 == strcmp(argv[i], "invert")){
                    InvertColors16bpp(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                }
                else if(0 == strcmp(argv[i], "ROTATE") || 0 == strcmp(argv[i], "rotate")){
                    imageBuffer_16 = rotate16bppImage(imageBuffer_16, bitmapInfoHeader.width, bitmapInfoHeader.height);
                    if(NULL == imageBuffer_16){
                        fprintf(stderr, "Rotation problem!");
                    }
                    INT32 temp = bitmapInfoHeader.width;
                    bitmapInfoHeader.width = bitmapInfoHeader.height;
                    bitmapInfoHeader.height = temp;
                }
            }
            
            write16bppImageToFile(bmpOutputFilePath, imageBuffer_16, bitmapFileHeader, bitmapInfoHeader, spaceBetweenHeadersAndImage);
            free(imageBuffer_16);
        }
        else{
            fprintf(stderr, "Not supported file format!");
            return 1;
        }
    }

    printFileHeaders(bitmapFileHeader, bitmapInfoHeader);

    return 0;
}