#include "bitmap.h"

///////////////////////////////////////////////////////////////////////////////
/// readPathsAndInstruction
///////////////////////////////////////////////////////////////////////////////
void readPaths(
    int argc,
    char** argv,
    char* inputFilePath,
    char* outputFilePath)
{
    if(argc > 1){
        strcpy(inputFilePath, argv[1]);
    }
    else{
        printf("Please enter input file path: ");
        scanf("%s", inputFilePath);
    }

    if(argc > 2){
        strcpy(outputFilePath, argv[2]);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// LoadFileMetaData
///////////////////////////////////////////////////////////////////////////////
FILE* LoadFileMetaData(
    char* filename,
    BMPINFOHEADER* BMPInforHeader,
    BMPFILEHEADER* BMPFileHeader,
    UINT8** space)
{
    FILE*     filePtr;

    //open file in read binary mode
    filePtr = fopen(filename, "rb");
    if (filePtr == NULL){
        fprintf(stderr, "file %s did not open", filename);
        return NULL;
    }
    //read the bitmap file header
    fread(BMPFileHeader, sizeof(BMPFILEHEADER), 1, filePtr);

    //verify that this is a .BMP file by checking bitmap id
    if (BMPFileHeader->type != 0x4D42)
    {
        fprintf(stderr, "Not a .BMP file!");
        fclose(filePtr);
        return NULL;
    }

    //read the bitmap info header
    fread(BMPInforHeader, sizeof(BMPINFOHEADER), 1, filePtr); 

    // allocate memory for the space between headers and image data
    // this space could contain color table and gap
    *space = (UINT8*)malloc(BMPFileHeader->offset - (sizeof(BMPINFOHEADER) + sizeof(BMPFILEHEADER)));
    if(NULL == space){
        fprintf(stderr, "Problem while allocating memory!");
        return NULL;
    }

    fread(*space, BMPFileHeader->offset - (sizeof(BMPINFOHEADER) + sizeof(BMPFILEHEADER)), 1, filePtr);

    return filePtr;
}

///////////////////////////////////////////////////////////////////////////////
/// Load1BppPixelMatrix
///////////////////////////////////////////////////////////////////////////////
Pixel_1** Load1BppPixelMatrix(
    FILE* filePtr,
    BMPINFOHEADER* BMPInforHeader,
    BMPFILEHEADER* BMPFileHeader)
{
    Pixel_1** bitmapImage;
    UINT32    widthInBytes;
    UINT8     padding = 4 - ((BMPInforHeader->width / 8) % 4);

    widthInBytes = ceil(BMPInforHeader->width * 1.0 / BITS_IN_BYTE);

    // allocate memory for the pixel matrix
    bitmapImage = (Pixel_1**)malloc(sizeof(Pixel_1*) * BMPInforHeader->height);
    if(NULL == bitmapImage){
        fprintf(stderr, "Problem while allocating memory for image buffer!");
        return NULL;
    }
    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        bitmapImage[i] = (Pixel_1*)calloc(widthInBytes, 1);
        if(NULL == bitmapImage[i]){
            fprintf(stderr, "Problem while allocating memory for image buffer!");
            return NULL;
        }
    }

    // read the pixel matrix
    fseek(filePtr, BMPFileHeader->offset, SEEK_SET);

    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        fread(bitmapImage[i], widthInBytes, 1, filePtr);
        fseek(filePtr, padding - 1, SEEK_CUR);
    }

    //close file and return bitmap image data
    fclose(filePtr);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// Load4BppPixelMatrix
///////////////////////////////////////////////////////////////////////////////
Pixel_4** Load4BppPixelMatrix(
    FILE* filePtr,
    BMPINFOHEADER* BMPInforHeader,
    BMPFILEHEADER* BMPFileHeader)
{
    Pixel_4** bitmapImage;
    UINT8  padding      = BMPInforHeader->width % 4;
    UINT32 widthInBytes = BMPInforHeader->width / 2 + padding;

    // allocate memory for the pixel matrix
    bitmapImage = (Pixel_4**)malloc(sizeof(Pixel_4*) * BMPInforHeader->height);
    if(NULL == bitmapImage){
        fprintf(stderr, "Problem while allocating memory for image buffer!");
        return NULL;
    }
    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        bitmapImage[i] = (Pixel_4*)calloc(widthInBytes, 1);
        if(NULL == bitmapImage[i]){
            fprintf(stderr, "Problem while allocating memory for image buffer!");
            return NULL;
        }
    }

    // read the pixel matrix
    fseek(filePtr, BMPFileHeader->offset, SEEK_SET);

    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        fread(bitmapImage[i], widthInBytes, 1, filePtr);
    }

    //close file and return bitmap image data
    fclose(filePtr);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// Load8BppBmpPixelMarix
///////////////////////////////////////////////////////////////////////////////
Pixel_8** Load8BppBmpPixelMarix(
    FILE* filePtr,
    BMPINFOHEADER* BMPInforHeader,
    BMPFILEHEADER* BMPFileHeader)
{
    Pixel_8** bitmapImage;

    //calculate the padding size
    UINT8 PADDING_SIZE = 4 - ((BMPInforHeader->width) % 4);
    if(4 == PADDING_SIZE){
        PADDING_SIZE = 0;
    }

    // allocate memory for the pixel matrix
    bitmapImage = (Pixel_8**)malloc(sizeof(Pixel_8*) * BMPInforHeader->height);
    if(NULL == bitmapImage){
        fprintf(stderr, "Problem while allocating memory for image buffer!");
        return NULL;
    }
    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        bitmapImage[i] = (Pixel_8*)calloc(BMPInforHeader->width, 1);
        if(NULL == bitmapImage[i]){
            fprintf(stderr, "Problem while allocating memory for image buffer!");
            return NULL;
        }
    }

    // read the pixel matrix
    fseek(filePtr, BMPFileHeader->offset, SEEK_SET);

    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        fread(bitmapImage[i], BMPInforHeader->width, 1, filePtr);
        fseek(filePtr, PADDING_SIZE, SEEK_CUR);
    }

    // close file and return bitmap image data
    fclose(filePtr);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// Load16BppBmpPixelMarix
///////////////////////////////////////////////////////////////////////////////
Pixel_16** Load16BppBmpPixelMarix(
    FILE* filePtr,
    BMPINFOHEADER* BMPInforHeader,
    BMPFILEHEADER* BMPFileHeader,
    UINT8** space)
{
    Pixel_16** bitmapImage;

    //calculate the padding size
    UINT8 PADDING_SIZE = 4 - ((BMPInforHeader->width * 2) % 4);
    if(4 == PADDING_SIZE){
        PADDING_SIZE = 0;
    }

    // allocating memory for image buffer
    bitmapImage = (Pixel_16**)malloc(sizeof(Pixel_16*) * BMPInforHeader->height);
    if(NULL == bitmapImage){
        fprintf(stderr, "Problem while allocating memory for image buffer!");
        return NULL;
    }
    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        bitmapImage[i] = (Pixel_16*)calloc(BMPInforHeader->width * sizeof(Pixel_16), 1);
        if(NULL == bitmapImage[i]){
            fprintf(stderr, "Problem while allocating memory for image buffer!");
            return NULL;
        }
    }

    // reading the image pixel matrix
    fseek(filePtr, BMPFileHeader->offset, SEEK_SET);

    for (UINT32 i = 0; i < BMPInforHeader->height; i++)
    {
        for (UINT32 j = 0; j < BMPInforHeader->width; j++)
        {
            fread(&bitmapImage[i][j], 2, 1, filePtr);
        }
        fseek(filePtr, PADDING_SIZE, SEEK_CUR);
    }

    //close file and return bitmap image data
    fclose(filePtr);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// flipImage1bpp
///////////////////////////////////////////////////////////////////////////////
void flipImage1bpp(
    Pixel_1** imageBuffer,
    INT32 width,
    INT32 height)
{
    UINT8 padding      = width % 8;
    INT32 widthInBytes = width / BITS_IN_BYTE;

    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < widthInBytes / 2  ; j++)
        {
            imageBuffer[i][j].pixelValue = reverseBits(imageBuffer[i][j].pixelValue);
            imageBuffer[i][widthInBytes - 1 - j].pixelValue = reverseBits(imageBuffer[i][widthInBytes - 1 - j].pixelValue);

            swap(imageBuffer[i][j], imageBuffer[i][widthInBytes - 1 - j]);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// flipImage4bpp
///////////////////////////////////////////////////////////////////////////////
void flipImage4bpp(
    Pixel_4** imageBuffer,
    INT32 width,
    INT32 height)
{
    INT32 widthInBytes = ceil(width * 1.0 / 2);

    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < widthInBytes / 2; j++)
        {
            imageBuffer[i][j].pixelValue = reverseBits(imageBuffer[i][j].pixelValue);
            imageBuffer[i][widthInBytes - 1 - j].pixelValue = reverseBits(imageBuffer[i][widthInBytes - 1 - j].pixelValue);

            swap(imageBuffer[i][j], imageBuffer[i][widthInBytes - 1 - j]);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// flipImage8bpp
///////////////////////////////////////////////////////////////////////////////
void flipImage8bpp(
    Pixel_8** imageBuffer,
    INT32 width,
    INT32 height)
{
    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < width / 2; j++)
        {
            swap(imageBuffer[i][j], imageBuffer[i][width - 1 - j]);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// flipImage16bpp
///////////////////////////////////////////////////////////////////////////////
void flipImage16bpp(
    Pixel_16** imageBuffer,
    INT32 width,
    INT32 height)
{
    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < width / 2; j++)
        {
            swap(imageBuffer[i][j], imageBuffer[i][width - 1 - j]);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// InvertColors1bpp
///////////////////////////////////////////////////////////////////////////////
void InvertColors1bpp(
    Pixel_1** imageBuffer,
    INT32 width,
    INT32 height)
{
    UINT8 padding = width % 8;

    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < width / 8 + padding; j++)
        {
            imageBuffer[i][j].pixelValue = 255 - imageBuffer[i][j].pixelValue;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// InvertColors4bpp
///////////////////////////////////////////////////////////////////////////////
void InvertColors4bpp(
    Pixel_4** imageBuffer,
    INT32 width,
    INT32 height)
{
    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < ceil(width * 1.0 / 2); j++)
        {
            imageBuffer[i][j].pixelValue = 255 - imageBuffer[i][j].pixelValue;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// InvertColors8bpp
///////////////////////////////////////////////////////////////////////////////
void InvertColors8bpp(
    Pixel_8** imageBuffer,
    INT32 width,
    INT32 height)
{
    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < width; j++)
        {
            imageBuffer[i][j].pixelValue = 255 - imageBuffer[i][j].pixelValue;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// InvertColors16bpp
///////////////////////////////////////////////////////////////////////////////
void InvertColors16bpp(
    Pixel_16** imageBuffer,
    INT32 width,
    INT32 height)
{
    for (UINT32 i = 0; i < height; i++)
    {
        for (UINT32 j = 0; j < width; j++)
        {
            imageBuffer[i][j].red = ~imageBuffer[i][j].red;
            imageBuffer[i][j].green = ~imageBuffer[i][j].green;
            imageBuffer[i][j].blue = ~imageBuffer[i][j].blue;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// rotate1bppImage
///////////////////////////////////////////////////////////////////////////////
Pixel_1** rotate1bppImage(
    Pixel_1** imageBuffer,
    INT32 width,
    INT32 height)
{
    Pixel_1** bitmapImage;

    bitmapImage = (Pixel_1**)malloc(width * 8);
    for (UINT32 i = 0; i < width; i++)
    {
        bitmapImage[i] = (Pixel_1*)calloc(height/8, 1);
    }

    Pixel_1* imageBufferCol = (Pixel_1*)calloc(height/8,1);
    for (UINT32 i = 0; i < width; i++)
    {
        for (UINT32 j = 0; j < height / 8; j++)
        {
            UINT8 eightPixelsUpwards = 0;
            UINT8 currBit = 0;
            for (UINT32 k = 0; k < 8; k++)
            {
                currBit = imageBuffer[j * 8 + k][(width / 8) - 1 - (i/8)].pixelValue & (1 << (i % 8));
                if(currBit){
                    eightPixelsUpwards = eightPixelsUpwards | (1 << (7-k)); 
                }
            }
            imageBufferCol[j].pixelValue = eightPixelsUpwards;
        }
        memcpy(bitmapImage[i], imageBufferCol, height/8);
    }
    free(imageBufferCol);

    free(imageBuffer);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// rotate4bppImage
///////////////////////////////////////////////////////////////////////////////
Pixel_4** rotate4bppImage(
    Pixel_4** imageBuffer,
    INT32 width,
    INT32 height)
{
    Pixel_4** bitmapImage;

    bitmapImage = (Pixel_4**)malloc(width * 8);
    for (UINT32 i = 0; i < width; i++)
    {
        bitmapImage[i] = (Pixel_4*)calloc(height/2, 1);
    }

    Pixel_4* imageBufferCol = (Pixel_4*)calloc(height/2,1);
    for (UINT32 i = 0; i < width; i++)
    {
        for (UINT32 j = 0; j < height / 2; j++)
        {
            UINT8 twoPixelsUpwards = 0;
            UINT8 currBit = 0;
            for (UINT32 k = 0; k < 4; k++)
            {
                currBit = imageBuffer[j * 2 + k][(width / 2) - 1 - (i/2)].pixelValue & (1 << (i % 2));
                if(currBit){
                    twoPixelsUpwards = twoPixelsUpwards | (1 << (7-k)); 
                }
            }
            imageBufferCol[j].pixelValue = twoPixelsUpwards;
        }
       memcpy(bitmapImage[i], imageBufferCol, height/2);
    }
    free(imageBufferCol);

    free(imageBuffer);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// rotate8bppImage
///////////////////////////////////////////////////////////////////////////////
Pixel_8** rotate8bppImage(
    Pixel_8** imageBuffer,
    INT32 width,
    INT32 height)
{
    Pixel_8** bitmapImage;

    bitmapImage = (Pixel_8**)malloc(width * sizeof(Pixel_8*));
    for (UINT32 i = 0; i < width; i++)
    {
        bitmapImage[i] = (Pixel_8*)calloc(height, 1);
    }

    Pixel_8* imageBufferCol = (Pixel_8*)calloc(height,1);
    for (UINT32 i = 0; i < width; i++)
    {
        for (UINT32 j = 0; j < height; j++)
        {
            imageBufferCol[j].pixelValue = imageBuffer[j][width - i -1].pixelValue;
        }
        memcpy(bitmapImage[i], imageBufferCol, height);
    }
    free(imageBufferCol);

    free(imageBuffer);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// rotate16bppImage
///////////////////////////////////////////////////////////////////////////////
Pixel_16** rotate16bppImage(
    Pixel_16** imageBuffer,
    INT32 width,
    INT32 height)
{
    Pixel_16** bitmapImage;

    bitmapImage = (Pixel_16**)malloc(width * sizeof(Pixel_16*));
    for (UINT32 i = 0; i < width; i++)
    {
        bitmapImage[i] = (Pixel_16*)calloc(height*3, 1);
    }

    for (UINT32 i = 0; i < width; i++)
    {
        for (UINT32 j = 0; j < height; j++)
        {
            memcpy(&bitmapImage[i][j], &imageBuffer[j][width - i - 1], 2);
        }
    }

    free(imageBuffer);

    return bitmapImage;
}

///////////////////////////////////////////////////////////////////////////////
/// write1bppImageToFile
///////////////////////////////////////////////////////////////////////////////
INT8 write1bppImageToFile(
    char* filePath,
    Pixel_1** imageBuffer, 
    BMPFILEHEADER bitmapFileHeader, 
    BMPINFOHEADER bitmapInfoHeader, 
    UINT8* space)
{
    UINT8 padding = 4 - ((bitmapInfoHeader.width / 8) % 4);

    FILE* fp = fopen(filePath, "wb");
    if(NULL == fp){
        fprintf(stderr, "File %s did not open correctly!", filePath);
        return 1;
    }

    fwrite(&bitmapFileHeader, sizeof(BMPFILEHEADER), 1, fp);
    fwrite(&bitmapInfoHeader, sizeof(BMPINFOHEADER), 1, fp);
    fwrite(space, bitmapFileHeader.offset - (sizeof(BMPFILEHEADER) + sizeof(BMPINFOHEADER)), 1, fp);

    fseek(fp, bitmapFileHeader.offset, SEEK_SET);
    for (UINT32 i = 0; i < bitmapInfoHeader.height; i++)
    {
        fwrite(imageBuffer[i], ceil(bitmapInfoHeader.width * 1.0 / BITS_IN_BYTE), 1, fp);
        fseek(fp, padding - 1, SEEK_CUR);
    }

    fclose(fp);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// write4bppImageToFile
///////////////////////////////////////////////////////////////////////////////
INT8 write4bppImageToFile(
    char* filePath,
    Pixel_4** imageBuffer, 
    BMPFILEHEADER bitmapFileHeader, 
    BMPINFOHEADER bitmapInfoHeader, 
    UINT8* space)
{
    UINT8  padding      = bitmapInfoHeader.width % 4;
    UINT32 widthInBytes = bitmapInfoHeader.width / 2 + padding;

    FILE* fp = fopen(filePath, "wb");
    if(NULL == fp){
        fprintf(stderr, "File %s did not open correctly!", filePath);
        return 1;
    }

    fwrite(&bitmapFileHeader, sizeof(BMPFILEHEADER), 1, fp);
    fwrite(&bitmapInfoHeader, sizeof(BMPINFOHEADER), 1, fp);
    fwrite(space, bitmapFileHeader.offset - (sizeof(BMPFILEHEADER) + sizeof(BMPINFOHEADER)), 1, fp);

    fseek(fp, bitmapFileHeader.offset, SEEK_SET);
    for (UINT32 i = 0; i < bitmapInfoHeader.height; i++)
    {
        fwrite(imageBuffer[i], widthInBytes, 1, fp);
    }

    fclose(fp);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// write8bppImageToFile
///////////////////////////////////////////////////////////////////////////////
INT8 write8bppImageToFile(
    char* filePath,
    Pixel_8** imageBuffer,
    BMPFILEHEADER bitmapFileHeader,
    BMPINFOHEADER bitmapInfoHeader,
    UINT8* space)
{
    FILE* fp = fopen(filePath, "wb");
    if(NULL == fp){
        fprintf(stderr, "File %s did not open correctly!", filePath);
        return 1;
    }

    fwrite(&bitmapFileHeader, sizeof(BMPFILEHEADER), 1, fp);
    fwrite(&bitmapInfoHeader, sizeof(BMPINFOHEADER), 1, fp);
    fwrite(space, bitmapFileHeader.offset - (sizeof(BMPFILEHEADER) + sizeof(BMPINFOHEADER)), 1, fp);

    UINT8 PADDING_SIZE = 4 - ((bitmapInfoHeader.width) % 4);
    if(4 == PADDING_SIZE){
        PADDING_SIZE = 0;
    }

    fseek(fp, bitmapFileHeader.offset, SEEK_SET);
    for (UINT32 i = 0; i < bitmapInfoHeader.height; i++)
    {
        fwrite(imageBuffer[i], bitmapInfoHeader.width, 1, fp);
        fseek(fp, PADDING_SIZE, SEEK_CUR);
    }

    fclose(fp);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// write16bppImageToFile
///////////////////////////////////////////////////////////////////////////////
INT8 write16bppImageToFile(
    char* filePath, 
    Pixel_16** imageBuffer, 
    BMPFILEHEADER bitmapFileHeader,
    BMPINFOHEADER bitmapInfoHeader, 
    UINT8* space)
{
    FILE* fp = fopen(filePath, "wb");
    if(NULL == fp){
        fprintf(stderr, "File %s did not open correctly!", filePath);
        return 1;
    }

    fwrite(&bitmapFileHeader, sizeof(BMPFILEHEADER), 1, fp);
    fwrite(&bitmapInfoHeader, sizeof(BMPINFOHEADER), 1, fp);

    fwrite(space, bitmapFileHeader.offset - (sizeof(BMPFILEHEADER) + sizeof(BMPINFOHEADER)), 1, fp);
    UINT8 PADDING_SIZE = 4 - ((bitmapInfoHeader.width * 2) % 4);
    if(4 == PADDING_SIZE){
        PADDING_SIZE = 0;
    }

    fseek(fp, bitmapFileHeader.offset, SEEK_SET);
    for (UINT32 i = 0; i < bitmapInfoHeader.height; i++)
    {
        for (UINT32 j = 0; j < bitmapInfoHeader.width; j++)
        {
            fwrite(&imageBuffer[i][j], 2, 1, fp);
        }

        fseek(fp, PADDING_SIZE, SEEK_CUR);
    }

    fclose(fp);
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// reverseBits
///////////////////////////////////////////////////////////////////////////////
UINT8 reverseBits(UINT8 n)
{
    UINT8 answer = 0;

    //first digit appended will be MSB at the end ensuring reverse
    for(UINT8 i = 0; i < 8; i++)
    {
        answer = (answer << 1) + (n >> i & 1);
    }

    return answer;
}

///////////////////////////////////////////////////////////////////////////////
/// printFileHeaders
///////////////////////////////////////////////////////////////////////////////
void printFileHeaders(
    BMPFILEHEADER bitmapFileHeader,
    BMPINFOHEADER bitmapInfoHeader)
{
    printf("BMP file header type: %d\n", bitmapFileHeader.type);
    printf("BMP file header size: %d\n", bitmapFileHeader.size);
    printf("BMP file header offset: %d\n", bitmapFileHeader.offset);
    printf("BMP info header size: %d\n", bitmapInfoHeader.size);
    printf("BMP info header width: %d\n", bitmapInfoHeader.width);
    printf("BMP info header height: %d\n", bitmapInfoHeader.height);
    printf("BMP info header planes: %d\n", bitmapInfoHeader.planes);
    printf("BMP info header bitsPerPixel: %d\n", bitmapInfoHeader.bitsPerPixel);
    printf("BMP info header compression: %d\n", bitmapInfoHeader.compression);
    printf("BMP info header imageSize: %d\n", bitmapInfoHeader.imageSize);
    printf("BMP info header xResolution: %d\n", bitmapInfoHeader.xResolution);
    printf("BMP info header yResolution: %d\n", bitmapInfoHeader.yResolution);
    printf("BMP info header nColours: %d\n", bitmapInfoHeader.nColours);
    printf("BMP info header importantColours: %d\n", bitmapInfoHeader.importantColours);
}

void printMenu(){
    printf("1: FLIP\n");
    printf("2: INVERT\n");
    printf("3: ROTATE\n");
    printf("4: SAVE\n");
}