#ifndef BITMAP_H
#define BITMAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//Aligns structure members on 1-byte boundaries, or on their natural alignment boundary, whichever is less.
#pragma pack(1)

//Swap macro
#define swap(x,y) do \ 
    { unsigned char swap_temp[sizeof(x) == sizeof(y) ? (signed)sizeof(x) : -1]; \
        memcpy(swap_temp, &y, sizeof(x)); \
        memcpy(&y, &x,        sizeof(x)); \
        memcpy(&x, swap_temp, sizeof(x)); \
    } while(0)

#define SHORT_STR_LENGHT 255
#define BITS_IN_BYTE 8

typedef unsigned char  UINT8;
typedef unsigned short UINT16;
typedef unsigned int   UINT32;
typedef char           INT8;
typedef short          INT16;
typedef int            INT32;

typedef enum{
    RESULT_SUCCESS = 0,
    RESULT_FAIL    = 1
}Results;

typedef struct {
    UINT16 type;      //specifies the file type
    UINT32 size;      //specifies the size in bytes of the bitmap file
    UINT16 reserved1; //reserved; must be 0
    UINT16 reserved2; //reserved; must be 0
    UINT32 offset;    //specifies the offset in bytes from the BMPFILEHEADER to the bitmap bits
} BMPFILEHEADER;

typedef struct {
    UINT32 size;             //specifies the number of bytes required by the struct
    INT32  width;            //specifies width in pixels
    INT32  height;           //specifies height in pixels
    UINT16 planes;           //specifies the number of color planes, must be 1
    UINT16 bitsPerPixel;     //specifies the number of bits per pixel
    UINT32 compression;      //specifies the type of compression
    UINT32 imageSize;        //size of image in bytes
    INT32  xResolution;      //number of pixels per meter in x axis
    INT32  yResolution;      //number of pixels per meter in y axis
    UINT32 nColours;         //number of colors used by the bitmap
    UINT32 importantColours; //number of colors that are important
} BMPINFOHEADER;

typedef struct {
    UINT8 pixelValue;
}Pixel_1;

typedef struct {
    UINT8 pixelValue : 2;
}Pixel_2;

typedef struct {
    UINT8 pixelValue;
}Pixel_4;

typedef struct {
    UINT8 pixelValue;
}Pixel_8;

typedef struct {
    UINT8 red : 5;
    UINT8 green : 6;
    UINT8 blue : 5;
}Pixel_16;

///////////////////////////////////////////////////////////////////////////////
/// @brief Reads the paths of the input and output files.
///
/// This function reads the paths from the command line.
/// If any of the parameters is not provided, it is red from the standart input instead.
///
/// @param argc           Count of the arguments.
/// @param argv           The value of the arguments.
/// @param inputFilePath  Input file path.
/// @param outputFilePath Output file path.
///////////////////////////////////////////////////////////////////////////////
void readPaths(int argc,
                char** argv,
                char* inputFilePath,
                char* outputFilePath);

///////////////////////////////////////////////////////////////////////////////
/// @brief Loads 1 bit per pixel bitmap file.
///
/// @param filename       Name of a file.
/// @param BMPInforHeader Bitmap info header.
/// @param BMPFileHeader  Bitmap file header.
/// @param space          Space between headers and image array.
///
/// @return pointer to FILE
///////////////////////////////////////////////////////////////////////////////
FILE* LoadFileMetaData(char *filename,
                    BMPINFOHEADER* BMPInforHeader,
                    BMPFILEHEADER* BMPFileHeader,
                    UINT8** space);


///////////////////////////////////////////////////////////////////////////////
/// @brief Loads 1 bit per pixel bitmap file.
///
/// @param filename       Name of a file.
/// @param BMPInforHeader Bitmap info header.
/// @param BMPFileHeader  Bitmap file header.
///
/// @return pointer to a pointer of Pixel_1
///////////////////////////////////////////////////////////////////////////////
Pixel_1** Load1BppPixelMatrix(FILE* filePtr,
                            BMPINFOHEADER* BMPInforHeader,
                            BMPFILEHEADER* BMPFileHeader);

///////////////////////////////////////////////////////////////////////////////
/// @brief Loads 4 bit per pixel bitmap file.
///
/// @param filename       Name of a file.
/// @param BMPInforHeader Bitmap info header.
/// @param BMPFileHeader  Bitmap file header.
///
/// @return pointer to a pointer of Pixel_4
///////////////////////////////////////////////////////////////////////////////
Pixel_4** Load4BppPixelMatrix(FILE* filePtr,
                            BMPINFOHEADER* BMPInforHeader,
                            BMPFILEHEADER* BMPFileHeader);

///////////////////////////////////////////////////////////////////////////////
/// @brief Loads 8 bit per pixel bitmap file.
///
/// @param filename       Name of a file.
/// @param BMPInforHeader Bitmap info header.
/// @param BMPFileHeader  Bitmap file header.
///
/// @return pointer to a pointer of Pixel_8
///////////////////////////////////////////////////////////////////////////////
Pixel_8** Load8BppBmpPixelMarix(FILE* filePtr,
                                BMPINFOHEADER* BMPInforHeader, 
                                BMPFILEHEADER* BMPFileHeader);

///////////////////////////////////////////////////////////////////////////////
/// @brief Loads 16 bit per pixel bitmap file.
///
/// @param filename       Name of a file.
/// @param BMPInforHeader Bitmap info header.
/// @param BMPFileHeader  Bitmap file header.
/// @param space          Space between headers and image array.
///
/// @return pointer to a pointer of Pixel_16
///////////////////////////////////////////////////////////////////////////////
Pixel_16** Load16BppBmpPixelMarix(FILE* filePtr,
                                BMPINFOHEADER* BMPInforHeader, 
                                BMPFILEHEADER* BMPFileHeader,
                                UINT8** space);


///////////////////////////////////////////////////////////////////////////////
/// @brief Reverse the bits of a number.
///
/// @param n number.
///
/// @return a number, which is the result of the reversed bits of n
///////////////////////////////////////////////////////////////////////////////
UINT8 reverseBits(UINT8 n);


///////////////////////////////////////////////////////////////////////////////
/// @brief Flips 1 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void flipImage1bpp(Pixel_1** imageBuffer,
                    INT32 width,
                    INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Flips 4 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void flipImage4bpp(Pixel_4** imageBuffer,
                    INT32 width,
                    INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Flips 8 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void flipImage8bpp(Pixel_8** imageBuffer,
                    INT32 width,
                    INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Flips 16 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void flipImage16bpp(Pixel_16** imageBuffer,
                    INT32 width,
                    INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Invert 1 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void InvertColors1bpp(Pixel_1** imageBuffer,
                        INT32 width,
                        INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Invert 4 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void InvertColors4bpp(Pixel_4** imageBuffer,
                        INT32 width,
                        INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Invert 8 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void InvertColors8bpp(Pixel_8** imageBuffer,
                        INT32 width,
                        INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Invert 16 bit per pixel bitmap image.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///////////////////////////////////////////////////////////////////////////////
void InvertColors16bpp(Pixel_16** imageBuffer,
                        INT32 width,
                        INT32 height);


///////////////////////////////////////////////////////////////////////////////
/// @brief Rotates 1 bit per pixel bitmap image by 90 degrees.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///
/// @return pointer to a pointer of Pixel_1
///////////////////////////////////////////////////////////////////////////////
Pixel_1** rotate1bppImage(Pixel_1** imageBuffer,
                            INT32 width,
                            INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Rotates 4 bit per pixel bitmap image by 90 degrees.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///
/// @return pointer to a pointer of Pixel_4
///////////////////////////////////////////////////////////////////////////////
Pixel_4** rotate4bppImage(Pixel_4** imageBuffer,
                            INT32 width,
                            INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Rotates 8 bit per pixel bitmap image by 90 degrees.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///
/// @return pointer to a pointer of Pixel_8
///////////////////////////////////////////////////////////////////////////////
Pixel_8** rotate8bppImage(Pixel_8** imageBuffer,
                            INT32 width,
                            INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Rotates 16 bit per pixel bitmap image by 90 degrees.
///
/// @param imageBuffer image buffer.
/// @param width       width of the image in pixels.
/// @param height      height of the image in pixels.
///
/// @return pointer to a pointer of Pixel_16
///////////////////////////////////////////////////////////////////////////////
Pixel_16** rotate16bppImage(Pixel_16** imageBuffer,
                            INT32 width,
                            INT32 height);

///////////////////////////////////////////////////////////////////////////////
/// @brief Writes 1 bit per pixel bitmap image to a file.
///
/// @param filePath         Path to the output file.
/// @param imageBuffer      Image buffer.
/// @param bitmapFileHeader Bitmap file header.
/// @param bitmapInfoHeader Bitmap info header.
/// @param space            Space between headers and image array.
///
/// @return 0 if everything is fine
///////////////////////////////////////////////////////////////////////////////
INT8 write1bppImageToFile(char* filePath,
                        Pixel_1** imageBuffer,
                        BMPFILEHEADER bitmapFileHeader,
                        BMPINFOHEADER bitmapInfoHeader,
                        UINT8* space);

///////////////////////////////////////////////////////////////////////////////
/// @brief Writes 4 bit per pixel bitmap image to a file.
///
/// @param filePath         Path to the output file.
/// @param imageBuffer      Image buffer.
/// @param bitmapFileHeader Bitmap file header.
/// @param bitmapInfoHeader Bitmap info header.
/// @param space            Space between headers and image array.
///
/// @return 0 if everything is fine
///////////////////////////////////////////////////////////////////////////////
INT8 write4bppImageToFile(char* filePath,
                        Pixel_4** imageBuffer,
                        BMPFILEHEADER bitmapFileHeader,
                        BMPINFOHEADER bitmapInfoHeader,
                        UINT8* space);

///////////////////////////////////////////////////////////////////////////////
/// @brief Writes 8 bit per pixel bitmap image to a file.
///
/// @param filePath         Path to the output file.
/// @param imageBuffer      Image buffer.
/// @param bitmapFileHeader Bitmap file header.
/// @param bitmapInfoHeader Bitmap info header.
/// @param space            Space between headers and image array.
///
/// @return 0 if everything is fine
///////////////////////////////////////////////////////////////////////////////
INT8 write8bppImageToFile(char* filePath,
                        Pixel_8** imageBuffer,
                        BMPFILEHEADER bitmapFileHeader,
                        BMPINFOHEADER bitmapInfoHeader,
                        UINT8* space);

///////////////////////////////////////////////////////////////////////////////
/// @brief Writes 16 bit per pixel bitmap image to a file.
///
/// @param filePath         Path to the output file.
/// @param imageBuffer      Image buffer.
/// @param bitmapFileHeader Bitmap file header.
/// @param bitmapInfoHeader Bitmap info header.
/// @param space            Space between headers and image array.
///
/// @return 0 if everything is fine
///////////////////////////////////////////////////////////////////////////////
INT8 write16bppImageToFile(char* filePath,
                            Pixel_16** imageBuffer,
                            BMPFILEHEADER bitmapFileHeader,
                            BMPINFOHEADER bitmapInfoHeader,
                            UINT8* space);

///////////////////////////////////////////////////////////////////////////////
/// @brief Prints the data of the file headers.
///
/// @param bitmapFileHeader Bitmap file header.
/// @param bitmapInfoHeader Bitmap info header.
///////////////////////////////////////////////////////////////////////////////
void printFileHeaders(BMPFILEHEADER bitmapFileHeader,
                        BMPINFOHEADER bitmapInfoHeader);

///////////////////////////////////////////////////////////////////////////////
/// @brief Prints menu.
///////////////////////////////////////////////////////////////////////////////
void printMenu();

#endif /* BITMAP_H */